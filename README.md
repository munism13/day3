## Prepare project structure 
1. mkdir < project name>
2. mkdir <project name>/server
3. mkdir <project name>/client
4. create app.js

## Prepare Git
1. (git init)
2. check on the current directory which remote is configured to (git remote -v)
3. To remove the git origin remote (git remote remove origin)
4. add origin (git remote add origin https://munism13@bitbucket.org/munism13/day3.git)
5. add files to local staging (git add .)
6. To commit files with commit remarks (git commit -m "commit remarks")
7. To push to repo (git push -u origin master)

## Global Utility installation
1. npm install -g nodemon
2. npm install -g bower 


## Prepare Express JS/ Nodemon
1. npm init
2. npm install express --save
3. nodemon 


## How to start my app
1. Mac - export NODE_PORT=3000 or Windows - set NODE_PORT=3000
2. nodemon or nodemon server/app.js

## How to test my app
1. Launch Chrome, point to http://localhost:3000